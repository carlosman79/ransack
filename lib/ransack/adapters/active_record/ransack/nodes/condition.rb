module Ransack
  module Nodes
    class Condition

      def arel_predicate   

        attributes.map { |attribute|

          association = attribute.parent
          if negative? && attribute.associated_collection?
            query = context.build_correlated_subquery(association)
            query.where(format_predicate(attribute).not)
            context.remove_association(association)
            Arel::Nodes::NotIn.new(context.primary_key, Arel.sql(query.to_sql))
          else
            format_predicate(attribute)
          end
        }.reduce(combinator_method)

      end

      private

        def combinator_method
          combinator === Constants::OR ? :or : :and
        end

        def format_predicate(attribute)
          arel_pred = arel_predicate_for_attribute(attribute)
          arel_values = formatted_values_for_attribute(attribute)
          puts arel_pred

          if (arel_pred.to_s=="lteq" || arel_pred.to_s=="gteq")
            Rails.logger.info 'datetime_format'
            arel_values =  arel_values.strftime(QueryReport.config.datetime_format)
            if arel_values.to_s.include?(':')
              arel_values =  arel_values.to_datetime.strftime(QueryReport.config.datetime_format)
            end
          elsif arel_values.kind_of?(Date)
              arel_values =  arel_values.strftime(QueryReport.config.datetime_format)
          end
          if (arel_pred.to_s=="in")
            predicate = attribute.attr.public_send(arel_pred, arel_values)
          else
            predicate = attribute.attr.public_send(arel_pred, arel_values.to_s)
          end
         
          
          if in_predicate?(predicate)
            predicate.right = predicate.right.map do |predicate|
              casted_array?(predicate) ? format_values_for(predicate) : predicate
            end
          end
          predicate
        end

        def in_predicate?(predicate)
          return unless defined?(Arel::Nodes::Casted)
          predicate.class == Arel::Nodes::In
        end

        def casted_array?(predicate)
          predicate.respond_to?(:val) && predicate.val.is_a?(Array)
        end

        def format_values_for(predicate)

          predicate.val.map do |value|
            value.is_a?(String) ? Arel::Nodes.build_quoted(value) : value
          end
        end

    end
  end
end
